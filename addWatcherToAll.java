/**
 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified
*/ 
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
    ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
    ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
    for (Issue issue : issues) {
        if (canWatchIssue(issue, currentUser, watcher)) {
            successfulIssues.add (issue);
} else { 
            failedIssues.add (issue);
        }
    }
    // 1st, We should be able to start Watching only if successfulIssues is "Not empty"
    if (!successfulIssues.isEmpty()) {
        // 2nd, If issuse is can watch, @param watcher should be the one that we add
        watcherManager.startWatching (watcher, successfulIssues);
    }
    return failedIssues;
}
private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
    if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
        return issue.getWatchingAllowed (watcher);
    }
    /* 3rd, If currentUser is not watchar and also does not has permission to modify,
       we should return false here.
    */
    return false;
}
